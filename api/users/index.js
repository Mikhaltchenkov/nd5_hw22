const express = require('express');
const app = module.exports = express();

let usersArray = [
    {id: 1, firstname: 'Ivan', lastname: 'Kreogenov', score: 34},
    {id: 2, firstname: 'Vasiliy', lastname: 'Smirnov', score: 35},
    {id: 3, firstname: 'Petr', lastname: 'Korovkin', score: 36}
];

function getUserById(id) {
    return usersArray.find(item => item.id == id );
}

app.get('/:id', (req, res) => {
    let userItem = getUserById(req.params.id);
    if (userItem) {
        res.json({'users': [userItem]});
    } else {
        res.status(404);
        res.json({'message': 'Object not found!'});
    }
});

app.get('/', (req, res) => {
    res.json({users: usersArray});
});

app.post('/', (req, res) => {
    if (req.body.firstname && req.body.lastname && req.body.score) {
        let id = usersArray.reduce((value, item) => (item.id > value)?item.id:value, 0) + 1;
        let userItem = {
            'id': id,
            'firstname': req.body.firstname,
            'lastname': req.body.lastname,
            'score': req.body.score
        };
        usersArray.push(userItem);
        res.json({'users': [userItem]});
    } else {
        res.status(400);
        res.json({'message': 'Fill all fields!'});
    }
});

app.put('/:id', (req, res) => {
    let userItem = getUserById(req.params.id);
    if (userItem) {
        userItem.firstname = req.body.firstname;
        userItem.lastname = req.body.lastname;
        userItem.score = req.body.score;
        res.json({users: [userItem]});
    } else {
        res.status(404);
        res.json({'message': 'Object not found!'});
    }
});

app.delete('/:id', (req, res) => {
    let userId = usersArray.findIndex(item => item.id == req.params.id);
    if (userId == -1) {
        res.status(404);
        res.json({'message': 'Object not found!'});
    } else {
        usersArray.splice(userId, 1);
        res.status(204);
        res.json({'message': 'ok!'});
    }
});


