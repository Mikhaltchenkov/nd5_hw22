const express = require('express');
const bodyParser = require('body-parser');
const api = require('./api');
const rpc = require('./rpc');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':true}));

// app.put('/:id', (req, res) => {
//         res.json({'firstname': req.body.firstname})
//     });


app.use('/api', api);

app.use('/rpc', rpc);

app.all('*', (req, res) => {
    res.send('ok');
});

app.listen(3000, () => {
    console.log('Server started');
});