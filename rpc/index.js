const express = require('express');
const app = module.exports = express();

let usersArray = [
    {id: 1, firstname: 'Ivan', lastname: 'Kreogenov', score: 34},
    {id: 2, firstname: 'Vasiliy', lastname: 'Smirnov', score: 35},
    {id: 3, firstname: 'Petr', lastname: 'Korovkin', score: 36}
];

const RPC = {
    listUsers: function(params, id, cb) {
        cb(null, usersArray);
    },
    showUser: function(params, id, cb) {
        if (id === undefined) {
            cb({'code': -32600, 'message': 'ID is undefined'});
        }
        let userItem = usersArray.find(item => item.id == id);
        if (userItem) {
            cb(null, userItem);
        } else {
            cb({'code': -32601, 'message': 'Object not found'});
        }
    },
    updateUser: function (params, id, cb) {
        if (id === undefined) {
            cb({'code': -32600, 'message': 'ID is undefined'});
        }
        try {
            params = JSON.parse(params);
        } catch (err) {
            cb({'code': -32600, 'message': 'Bad request'});
        }
        let userItem = usersArray.find(item => item.id == id);
        if (userItem) {
            userItem.firstname = params.firstname;
            userItem.lastname = params.lastname;
            userItem.score = params.score;
            cb(null, userItem);
        } else {
            cb({'code': -32601, 'message': 'Object not found'});
        }
    },
    createUser: function (params, id, cb) {
        try {
            params = JSON.parse(params);
        } catch (err) {
            cb({'code': -32600, 'message': 'Bad request'});
        }
        if (params.firstname && params.lastname && params.score) {
            let id = usersArray.reduce((value, item) => (item.id > value)?item.id:value, 0) + 1;
            let userItem = {
                'id': id,
                'firstname': params.firstname,
                'lastname': params.lastname,
                'score': params.score
            };
            usersArray.push(userItem);
            cb(null, userItem);
        } else {
            cb({'code': -32600, 'message': 'Fill all fields!'});
        }
    },
    deleteUser: function (params, id, cb) {
        if (id === undefined) {
            cb({'code': -32600, 'message': 'ID is undefined'});
        }
        let userId = usersArray.findIndex(item => item.id == id);
        if (userId == -1) {
            cb({'code': -32601, 'message': 'Object not found'});
        } else {
            usersArray.splice(userId, 1);
            cb(null, 'ok!');
        }
    }
};

app.post("/", function(req, res) {
    if (req.body.jsonrpc != "2.0") {
        res.json({
            "jsonrpc": "2.0",
            "error": {"code": -32600, "message": "Invalid JSONRPC version request"},
            "id": req.body.id
        });
    } else {
        if (!Object.keys(RPC).find(el => el == req.body.method && typeof RPC[el] == 'function')) {
            res.json({"jsonrpc": "2.0", "error": {"code": -32601, "message": "Method not found"}, "id": req.body.id});
        } else {
            let id = req.body.id === undefined ? null : req.body.id;
            let method = RPC[req.body.method];
            method(req.body.params, id, function (error, result) {
                if (error) {
                    let answer = {
                        "jsonrpc": "2.0",
                        "error": error,
                        "id": id
                    };
                    res.json(answer);
                } else {
                    let answer = {
                        "jsonrpc": "2.0",
                        "result": result,
                        "id": id
                    };
                    res.json(answer);
                }
            });
        }
    }
});